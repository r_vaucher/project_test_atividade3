

## Este é um projeto fictício, foi criado para acompanhamento de atividade de cadeira de pós-graduação.

O projeto consiste no desenvolvimento e manutenção de um aplicativo para utilização em dispositivos Android.

Tendo como funcionalidade principal permitir ao usuário, pesquisar o resultado de seus exames laboratoriais diretamente no dispositivo em questão, podendo salva-los localmente ou compartilha-lo com o seu médico através de permissão visualização especial.

O interfaceamento com a unidade executora e responsável pela disponibilidade dos exames será realizado através de webservice.
   
O tipo de licença de software utilizado neste projeto é a MPL 2.0.
Mais informações podem ser encontradas no arquivo LICENSE.rtf